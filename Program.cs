﻿using System;
using System.IO;
using System.Text;

namespace T_VersionEncoder
{
    class Program
    {
        const byte йоу = 2;
        static void Main(string[] args)
        {
            uint[] ч = new uint[йоу] { uint.Parse(ъ("version pc (uint): ")), uint.Parse(ъ("version android (uint): ")) };
            ushort[][] з = new ushort[йоу][];
            string[] йоусы = new string[йоу] { "pc ya link", "android ya link" };
            for (int ф = 0; ф < йоу; ф++)
            {
                string с = ъ(йоусы[ф] + ": ");
                ushort[] ц = г(Encoding.Unicode.GetBytes(с));
                string t = Encoding.Unicode.GetString(ю(ц));
                if (с != t)
                    ь($"got different links:\n{с}\n{t}");
                з[ф] = ц;
            }
            using (FileStream й = File.OpenWrite("version.txt"))
            using (BinaryWriter у = new BinaryWriter(й, Encoding.ASCII))
            {
                у.Write((byte)йоу);
                foreach (uint гы in ч)
                    у.Write((uint)гы);
                foreach (ushort[] цы in з)
                {
                    у.Write((ushort)цы.Length);
                    foreach (ushort ы in цы) у.Write((ushort)ы);
                }
            }
        }

        static string ъ(string s) { Console.Write(s); return Console.ReadLine(); }
        static void ь(string s) { Console.Write(s); Console.ReadLine(); Environment.Exit(0); }

        static ushort[] г(byte[] б)
        {
            ushort[] r = new ushort[б.Length];
            for (int i = 0; i < б.Length; i++)
            {
                if (б[i] > 128) ь($"data[{i}] is > 128 = {б[i]}");
                r[i] = (ushort)(б[i] * 2);
            }
            return r;
        }

        static byte[] ю(ushort[] б)
        {
            byte[] r = new byte[б.Length];
            for (int i = 0; i < б.Length; i++) r[i] = (byte)(б[i] / 2);
            return r;
        }

    }
}
